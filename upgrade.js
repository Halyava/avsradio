#!/bin/node

global.__root = __dirname;
global.__module = 'avsr.upgrade';

const path = require('path');
const log = require(`${__root}/api/util/logger`);
const TAG = path.basename(__filename);

log.d(TAG, 'Start upgrading...');

exec("systemctl restart avsr", (error, stdout, stderr) => {
    if (error) {
        log.e(TAG, stderr);
    }
});

log.d(TAG, 'Upgrade was successfully completed.');
